//
//  Date.swift
//  desafio-ios
//
//  Created by Henrique Santos on 28/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import Foundation


extension Date {
    func toString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: self)
    }
    
    static func formatDateString(currentDate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:sszzz"
        
        if let date = dateFormatter.date(from:currentDate) {
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let dateString = dateFormatter.string(from:date)
            return dateString
        }
        return currentDate
    }
}
