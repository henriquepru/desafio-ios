//
//  PullRequest.swift
//  desafio-ios
//
//  Created by Henrique Santos on 26/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import Foundation
import ObjectMapper

struct PullRequest {
    var title = ""
    var description = ""
    var user = User()
    var urlString = ""
    var date = ""
}

extension PullRequest : Mappable {
    
    init?(map: Map) {
        mapping(map: map)
    }
    
    mutating func mapping(map: Map) {
        title <- map["title"]
        description <- map["body"]
        user <- map["user"]
        urlString <- map["html_url"]
        date <- (map["created_at"])
    }
}


