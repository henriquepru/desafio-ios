//
//  PullRequestService.swift
//  desafio-ios
//
//  Created by Henrique Santos on 27/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class PullRequestService: NSObject {
    enum PullRequestServiceDataResult {
        case Success([PullRequest])
        case Failure(Error)
    }
    
    static func fetchPullRequest(owner: String, repo : String, page: Int, completion : @escaping (PullRequestServiceDataResult) -> Void) {
        Alamofire.request(PullRequestsRepo.getPullRequests(userName: owner, repoName: repo, page: page)).responseJSON { (response) in
            
            if let error = response.error {
                completion(.Failure(error))
                
            }else if let json =  response.result.value as? [[String : Any]] {
                 let pullRequests =  Mapper<PullRequest>().mapArray(JSONArray: json)
                completion(.Success(pullRequests))
            }
            
        }
    }
    
}
