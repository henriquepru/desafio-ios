//
//  PullRequestTableViewCell.swift
//  desafio-ios
//
//  Created by Henrique Santos on 27/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell, ReusableView, NibLoadableView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var createAt: UILabel!
    
    func setup(pullRequest: PullRequest) {
        titleLabel.text = pullRequest.title
        descriptionLabel.text = pullRequest.description
        usernameLabel.text = pullRequest.user.username
        fullNameLabel.text = pullRequest.user.fullName
        profileImage.download(image: pullRequest.user.avatarUrl)
        createAt.text = Date.formatDateString(currentDate: pullRequest.date)
    }
}
