//
//  PullRequestsRepo.swift
//  desafio-ios
//
//  Created by Henrique Santos on 27/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import UIKit
import Alamofire

public enum PullRequestsRepo: URLRequestConvertible  {
    case getPullRequests(userName: String, repoName: String, page: Int)
    
    static let baseURLPath = "https://api.github.com"
    
    public func asURLRequest() throws -> URLRequest {
        let result : (path: String, parameters: Parameters) = {
            switch self {
            case let .getPullRequests(userName, repoName,page) where page > 0:
                return ("/repos/\(userName)/\(repoName)/pulls", ["page": page])
            case let .getPullRequests(userName, repoName, _):
                return ("/repos/\(userName)/\(repoName)/pulls",[:])
            }
        }()
        
        let url = try RepositoriesRepo.baseURLPath.asURL()
        let urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        
        return try URLEncoding.default.encode(urlRequest, with: result.parameters)
    }
}

