//
//  PullRequestsTableViewController.swift
//  desafio-ios
//
//  Created by Henrique Santos on 28/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import UIKit

class PullRequestsTableViewController: UITableViewController {

    var pullRequests : [PullRequest] = [] {
        didSet{ tableView.reloadData() }
    }
    
    var repository = Repository()
    
    var currentPage = 1
    var isFinishLoad = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = repository.name
        
        tableView.register(PullRequestTableViewCell.self)
        fetchPullRequests(ownerUsername: repository.owner.username, repositoryName: repository.name, page: currentPage)
    }
    
 
}
// MARK: - Support Methods
extension PullRequestsTableViewController {
    func fetchPullRequests(ownerUsername : String, repositoryName: String, page: Int ){
        startActivityIndicator(tag: 1)
        
        PullRequestService.fetchPullRequest(owner: ownerUsername, repo: repositoryName, page: currentPage) { (pullRequestServiceDataResult) in
            
            switch pullRequestServiceDataResult {
            case .Success(let pullRequests) :
                self.stopActivityIndicator(tag: 1)
                
                if self.checkEndItens(countItens: pullRequests.count) {
                    self.isFinishLoad = true
                }
                self.pullRequests.append(contentsOf: pullRequests)
                
            case .Failure(let erro) :
                print(erro.localizedDescription)
                self.stopActivityIndicator(tag: 1)
            }
        }
    }
    
    func openPullRequesOnBrowser(urlString : String) {
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
    }
    
    func checkEndItens(countItens: Int) -> Bool {
        return countItens == 0
    }
}

// MARK: - UITableViewDelegate
extension PullRequestsTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openPullRequesOnBrowser(urlString: pullRequests[indexPath.row].urlString)
    }
}

// MARK: - UITableViewDataSource
extension PullRequestsTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequests.count 
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : PullRequestTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        
        cell.setup(pullRequest: pullRequests[indexPath.row])
        
        if indexPath.row == pullRequests.count-1 && !isFinishLoad {
            currentPage += 1
             fetchPullRequests(ownerUsername: repository.owner.username, repositoryName: repository.name, page: currentPage)
        }
        
        return cell
    }
}


