//
//  RepositoryRepo.swift
//  desafio-ios
//
//  Created by Henrique Santos on 26/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import Foundation
import Alamofire

public enum RepositoriesRepo: URLRequestConvertible  {
    case getRepositories(query: String, page: Int)
    
    static let baseURLPath = "https://api.github.com"
    
    public func asURLRequest() throws -> URLRequest {
        let result : (path: String, parameters: Parameters) = {
            switch self {
            case let .getRepositories(query, page) where page > 0:
                return ("/search/repositories", ["q": query, "page": page])
            case let .getRepositories(query, _):
                return ("/search/repositories", ["q": query, "page": 1])
            }
        }()
        
        let url = try RepositoriesRepo.baseURLPath.asURL()
        let urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        
        return try URLEncoding.default.encode(urlRequest, with: result.parameters)
    }
}

