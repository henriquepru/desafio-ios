//
//  ViewController.swift
//  desafio-ios
//
//  Created by Henrique Santos on 26/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper


struct RepositoriesTableViewControllerConstants {
    struct SegueIdentifiers {
        static let PullRequests = "PullRequestsTableViewController"
    }
    struct Messages {
        static let ErrorTitle = "Erro"
        static let InternalError = "Algo deu errado :("
    }
}


class RepositoriesTableViewController: UITableViewController {
    
    var currentPage = 1
    var isFinishLoad = false
    
    var repositories : [Repository] = [] {
        didSet { tableView.reloadData() }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(RepositoryTableViewCell.self)
        fetchRepositories(query:RepositoryServiceLanguages.Java,page: currentPage)
    }
    

}
// MARK: - Support Methods
extension RepositoriesTableViewController {
    
    func fetchRepositories(query: String, page: Int ){
        startActivityIndicator(tag: 1)
        RepositoryService.fetchAllRespositories(query: query, page: page) {
            (repositoryServiceDataResult) in
            
            switch repositoryServiceDataResult {
            case .Success(let repositories) :
                if self.checkEndItens(countItens: repositories.count) {
                    self.isFinishLoad = true
                }
                self.repositories.append(contentsOf: repositories)
                self.stopActivityIndicator(tag: 1)
            case .Failure( _) :
                self.showAlerError(title: RepositoriesTableViewControllerConstants.Messages.ErrorTitle, message: RepositoriesTableViewControllerConstants.Messages.InternalError)
                self.stopActivityIndicator(tag: 1)
                
            }
        }
    }
    
    func checkEndItens(countItens: Int) -> Bool {
        return countItens == 0
    }
}

// MARK: - UITableViewDataSource
extension RepositoriesTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : RepositoryTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.setup(repository: repositories[indexPath.row])
        
        if indexPath.row == repositories.count-1 && !isFinishLoad {
            currentPage += 1
            fetchRepositories(query:RepositoryServiceLanguages.Java, page: currentPage)
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension RepositoriesTableViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: RepositoriesTableViewControllerConstants.SegueIdentifiers.PullRequests, sender: repositories[indexPath.row])
    }
}


// MARK: - Segue
extension RepositoriesTableViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let identifier = segue.identifier {
            switch identifier {
            case RepositoriesTableViewControllerConstants.SegueIdentifiers.PullRequests:
                if let destinationController = segue.destination as? PullRequestsTableViewController {
                    destinationController.repository = sender as? Repository ?? Repository()
                }
   
            default:
                showAlerError(title: RepositoriesTableViewControllerConstants.Messages.ErrorTitle, message: RepositoriesTableViewControllerConstants.Messages.InternalError)
            }
        }
    }
}






