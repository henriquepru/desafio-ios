//
//  Repository.swift
//  desafio-ios
//
//  Created by Henrique Santos on 26/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import Foundation
import ObjectMapper

struct Repository {
    var name = ""
    var description = ""
    var forksCount = 0
    var starsCount = 0
    var owner = User()
}

extension Repository : Mappable {
    
    init?(map: Map) {
        mapping(map: map)
    }
    
    mutating func mapping(map: Map) {
        
        name <- map["name"]
        description <- map["description"]
        forksCount <- map["forks"]
        starsCount <- map["stargazers_count"]
        owner <- map["owner"]
    }
}
