//
//  RepositoryService.swift
//  desafio-ios
//
//  Created by Henrique Santos on 27/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

//Constant para filtrar as Linguagens atraves das querys
struct RepositoryServiceLanguages {
    static let Java = "language:Java"
}

class RepositoryService: NSObject {
    
    enum RepositoryServiceDataResult {
        case Success([Repository])
        case Failure(Error)
    }
    
    static func fetchAllRespositories(query: String, page : Int, completion : @escaping (RepositoryServiceDataResult) -> Void ) {
        
        Alamofire.request(RepositoriesRepo.getRepositories(query: query, page: page)).responseJSON { (response) in
            
            if let error = response.error {
                completion(.Failure(error))
                
            }else if let json = response.result.value as? [String : Any], let items =  json["items"] as?[[String :Any]] {
                let repositories =  Mapper<Repository>().mapArray(JSONArray: items)
                completion(.Success(repositories))
            }
        }
    }

}
