//
//  RepositoryTableViewCell.swift
//  desafio-ios
//
//  Created by Henrique Santos on 27/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell, ReusableView, NibLoadableView  {
    
    @IBOutlet weak var repositoryNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var ownerUserNameLabel: UILabel!
    @IBOutlet weak var ownerFullNameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    func setup(repository: Repository) {
        repositoryNameLabel.text = repository.name
        descriptionLabel.text = repository.description
        forksLabel.text = "\(repository.forksCount)"
        starsLabel.text = "\(repository.starsCount)"
        ownerUserNameLabel.text = repository.owner.username
        ownerFullNameLabel.text = repository.owner.fullName
        profileImage.download(image: repository.owner.avatarUrl)
    }
}


