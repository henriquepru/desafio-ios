//
//  ReusableView.swift
//  desafio-ios
//
//  Created by Henrique Santos on 27/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import UIKit

protocol ReusableView : class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}
