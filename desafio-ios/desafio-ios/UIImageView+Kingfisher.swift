//
//  UIImageView+Kingfisher.swift
//  desafio-ios
//
//  Created by Henrique Santos on 28/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func download(image url: String) {
        if let imageURL = URL(string:url) {
            self.kf.setImage(with: ImageResource(downloadURL: imageURL))
        }
    }
}
