//
//  UIViewController+ActivityIndicator.swift
//  desafio-ios
//
//  Created by Henrique Santos on 28/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import UIKit


extension UIViewController {
    
    func startActivityIndicator(tag : Int, style: UIActivityIndicatorViewStyle = .gray, location: CGPoint? = nil) {
        
        let loc = location ?? self.view.center
        
        DispatchQueue.main.async(execute: {
            
            let activityIndicator =
                UIActivityIndicatorView(activityIndicatorStyle: style)
            activityIndicator.tag = tag
            activityIndicator.center = loc
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
        })
    }
    
    func stopActivityIndicator(tag: Int) {
        
         DispatchQueue.main.async(execute: {
            if let activityIndicator = self.view.subviews.filter(
                { $0.tag == tag}).first as? UIActivityIndicatorView {
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            }
        })
    }
    

}
