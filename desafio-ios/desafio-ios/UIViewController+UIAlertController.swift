//
//  UIViewController+UIAlertController.swift
//  desafio-ios
//
//  Created by Henrique Santos on 28/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlerError(title: String, message : String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "Close", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}


