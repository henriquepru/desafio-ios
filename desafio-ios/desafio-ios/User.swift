//
//  Owner.swift
//  desafio-ios
//
//  Created by Henrique Santos on 27/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import Foundation
import ObjectMapper

struct User {
    var username = ""
    var fullName = ""
    var avatarUrl = ""
}

extension User : Mappable {
    
    init?(map: Map) {
        mapping(map: map)
    }
    
    mutating func mapping(map: Map) {
        username <- map["login"]
        fullName <- map["login"]
        avatarUrl <- map["avatar_url"]
    }
}
