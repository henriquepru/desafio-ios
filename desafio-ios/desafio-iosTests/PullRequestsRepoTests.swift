//
//  PullRequestsRepoTests.swift
//  desafio-ios
//
//  Created by Henrique Santos on 29/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import Quick
import Nimble
@testable import desafio_ios

class PullRequestsRepoTests: QuickSpec {
    
    override func spec() {
        
        describe("a repository PullRequests") {
            context("static context", {
                
                it("Should be url PullRequest nil", closure: {
                    expect(PullRequestsRepo.baseURLPath).to(equal("https://api.github.com"))
                    expect(PullRequestsRepo.baseURLPath).toNot(equal("http:/api.github.com"))
                })
            })
            
            context("url reponse format", {
                let urlRequest  = try? PullRequestsRepo.getPullRequests(userName: "elastic", repoName: "elasticsearch", page: 1).asURLRequest()
                
                it("should return a urlRequest", closure: {
                    expect(urlRequest).to(beAKindOf(URLRequest.self))
                })
                
                it("should be equal a asolute url", closure: {
                    expect(urlRequest?.url?.absoluteString).to(equal("https://api.github.com/repos/elastic/elasticsearch/pulls?page=1"))
                })
            })
        }
    }
    

}
