//
//  PullRequestsTableViewControllerTests.swift
//  desafio-ios
//
//  Created by Henrique Santos on 29/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import Quick
import Nimble
@testable import desafio_ios

class PullRequestsTableViewControllerTests: QuickSpec {
    
    override func spec() {
        
        describe("pullrequests TableViewController") {
            
            var controller : PullRequestsTableViewController!
            
            beforeEach {
                let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                controller = storyboard.instantiateViewController(withIdentifier: "PullRequestsTableViewController") as! PullRequestsTableViewController
            }
            
            it("should be able to create a controller") {
                expect(controller).toNot(beNil())
            }
            
            it("should have a view of type") {
                expect(controller).to(beAKindOf(PullRequestsTableViewController.self))
            }
            
            context("static context", {
                
                it("should not be null", closure: {
                    expect(controller.currentPage).toNot(beNil())
                    expect(controller.isFinishLoad).toNot(beNil())
                    expect(controller.repository).toNot(beNil())
                })
                
            })

        }
    }
}
