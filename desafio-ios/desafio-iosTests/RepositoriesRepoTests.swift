//
//  RepositoriesRepoTest.swift
//  desafio-ios
//
//  Created by Henrique Santos on 29/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import Quick
import Nimble
@testable import desafio_ios

class RepositoriesRepoTests: QuickSpec {
    override func spec() {
        
        describe("a repository repositories") {
            context("static context", {
                
                it("should be url Repository equal describe url", closure: {
                    expect(RepositoriesRepo.baseURLPath).to(equal("https://api.github.com"))
                    expect(RepositoriesRepo.baseURLPath).toNot(equal("http:/api.github.com"))
                })
            })
            
            context("url reponse format", {
                 let urlRequest  = try? RepositoriesRepo.getRepositories(query: "language:java", page: 1).asURLRequest()
                it("should return a urlRequest", closure: {
                    expect(urlRequest).to(beAKindOf(URLRequest.self))
                })
                
                it("should be equal a asolute url", closure: {
                    expect(urlRequest?.url?.absoluteString).to(equal("https://api.github.com/search/repositories?page=1&q=language%3Ajava"))
                })
            })
        }
    }

}
