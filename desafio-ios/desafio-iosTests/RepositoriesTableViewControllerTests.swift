//
//  RepositoriesTableViewControllerTests.swift
//  desafio-ios
//
//  Created by Henrique Santos on 29/05/17.
//  Copyright © 2017 Henrique Prudencio. All rights reserved.
//

import Quick
import Nimble
@testable import desafio_ios

class RepositoriesTableViewControllerTests: QuickSpec {
    
    override func spec() {
        
        describe("repositories TableViewController") {
            
            var controller : RepositoriesTableViewController!
            
            beforeEach {
                let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                let navigation = storyboard.instantiateInitialViewController() as! UINavigationController
                controller = navigation.viewControllers.first as! RepositoriesTableViewController
            }
            
            it("should be able to create a controller") {
                expect(controller).toNot(beNil())
            }
            
            it("should have a view of type") {
                expect(controller).to(beAKindOf(RepositoriesTableViewController.self))
            }
            
            context("static context", {
                
                it("should not be null", closure: { 
                    expect(controller.currentPage).toNot(beNil())
                    expect(controller.isFinishLoad).toNot(beNil())
                    expect(controller.repositories).toNot(beNil())
                })
                
                it("should has the initial values", closure: { 
                    expect(controller.currentPage).to(equal(1))
                    expect(controller.isFinishLoad).to(equal(false))
                })
            })
        }
    }
}
